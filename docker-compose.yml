version: "3.8"

x-default-healthcheck: &default-healthcheck
  interval: 10s
  timeout: 2s
  retries: 3

services:
  zookeeper:
    image: bitnami/zookeeper:latest
    container_name: zookeeper
    networks:
      app-network:
        ipv4_address: 172.26.1.1
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes
    ports:
      - "2181:2181"
    volumes:
      - zookeeper-data:/zookeeper_data
    healthcheck:
      <<: *default-healthcheck
      test: ["CMD", "/opt/bitnami/zookeeper/bin/zkServer.sh", "status"]

  kafka:
    image: bitnami/kafka:latest
    container_name: kafka
    networks:
      app-network:
        ipv4_address: 172.26.1.2
    environment:
      - KAFKA_BROKER_ID=1
      - KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper:2181
      - ALLOW_PLAINTEXT_LISTENER=yes
      - KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CLIENT:PLAINTEXT,EXTERNAL:PLAINTEXT
      - KAFKA_CFG_LISTENERS=CLIENT://:9092,EXTERNAL://:9093
      - KAFKA_CFG_ADVERTISED_LISTENERS=CLIENT://kafka:9092,EXTERNAL://kafka:9093
      - KAFKA_INTER_BROKER_LISTENER_NAME=CLIENT
    ports:
      - "9092:9092"
    volumes:
      - kafka-data:/kafka_data
    depends_on:
      zookeeper:
        condition: "service_healthy"
    healthcheck:
      <<: *default-healthcheck
      test:
        [
          "CMD",
          "/opt/bitnami/kafka/bin/zookeeper-shell.sh",
          "zookeeper:2181",
          "get",
          "/brokers/ids/1",
        ]

  postgres:
    image: postgres
    container_name: postgres
    networks:
      app-network:
        ipv4_address: 172.26.1.3
    environment:
      POSTGRES_PASSWORD: postgres
    user: postgres
    ports:
      - "5432:5432"
    volumes:
      - postgres-data:/postgres-data
    healthcheck:
      test: ["CMD-SHELL", "pg_isready", "-U", "postgres", "-d", "postgres"]
      interval: 30s
      timeout: 60s
      retries: 5
      start_period: 20s

  adminer:
    image: adminer
    container_name: adminer
    networks:
      app-network:
        ipv4_address: 172.26.1.4
    ports:
      - "8082:8080"

  webserver:
    image: spkws:latest
    container_name: spkws
    networks:
      app-network:
        ipv4_address: 172.26.1.0
    restart: always
    ports:
      - "8080:8080"
    depends_on:
      kafka:
        condition: "service_healthy"
      postgres:
        condition: "service_healthy"

volumes:
  zookeeper-data:
  kafka-data:
  postgres-data:

networks:
  app-network:
    name: app-network
    driver: "bridge"
    ipam:
      driver: default
      config:
        - subnet: 172.26.0.0/16
          gateway: 172.26.0.1
