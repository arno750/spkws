.DEFAULT_GOAL := build

help: ## This help message
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\033[1;34m\1\\033[0;39m:\2/' | column -c2 -t -s :)"

clean: ## Clean
	mvn clean

package: ## Package the application	
	./mvnw package

docker: ## Build the docker
	docker build -t spkws .

build: clean package docker ## Package the application, then build the docker

up: ## Start docker-compose
	docker compose up --detach

down: ## Stop docker-compose
	docker compose down

consume: ## Consume Kafka topic
	docker exec -it $$(docker container ls | grep 'kafka' | awk '{print $$1}') ./opt/bitnami/kafka/bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic racing-notif --from-beginning

test: ## Test the application
	./mvnw test

run: ## Run the application
	java -jar target/spkws-$$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout).jar
