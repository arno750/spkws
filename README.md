# Développement d'un MVP avec Spring Boot, PostgreSQL et Kafka

## Développement

### Initialisation du projet Spring

Via [Spring Initializ](https://start.spring.io/), génération d'un projet Maven Java 11 basé sur Spring Boot 2.7.0 contenant :

- Spring Web
- Spring Data JPA
- Spring for Apache Kafka
- Validation

### Requirements

Versions utilisées des différents outils de développement :

- OpenJDK 11.0.17
- Apache Maven 3.8.4
- GNU Make 4.3
- Docker 20.10.21
- Docker Compose 2.12.2

### Modèle de données

```mermaid
classDiagram

class Race {
  -Date date
  -String name
  -Integer number
  -List~Runner~ runners
}
class Runner {
  -String name
  -Integer number
}

Race --> "3..*" Runner : contains
```

### Modèle de base de données

```mermaid
classDiagram

class Race {
  date
  name
  number
}
class Runner {
  name
  number
  race_date
  race_number
}

Race <-- Runner
```

La table `Race` a un index primaire basé sur `date`, `name`.
La table `Runner` a un index primaire basé sur `name`, `race_date`, `race_name`.
Il est possible d'effectuer une jointure entre les deux tables en reliant conjointement :

- `Race.date` à `Runner.race_date`
- `Race.number` à `Runner.race_number`

## API REST

La spécification Open API est disponible sous la forme d'un fichier swagger auto généré. L'API REST est testable via un Swagger UI.

Ci-dessous les URLs selon que l'application est lancée en local ou via `docker-compose` :

|       URL       |                  en local                   |              via docker-compose              |
| :-------------: | :-----------------------------------------: | :------------------------------------------: |
| Fichier swagger |     http://localhost:8080/v3/api-docs/      |     http://172.26.1.0:8080/v3/api-docs/      |
|   Swagger UI    | http://localhost:8080/swagger-ui/index.html | http://172.26.1.0:8080/swagger-ui/index.html |

## Configuration

### Ports

|       Composant        |   Port    |
| :--------------------: | :-------: |
| Spring boot web server |   8080    |
|      Apache Kafka      |   9092    |
|    Apache ZooKeeper    |   2181    |
|       PostgreSQL       |   5432    |
|        Adminer         | 8082:8080 |

Note: Adminer est sur le port 8080 dans le docker mais est exposé sur le port 8082 dans l'hôte.

### Base de données SQL

|    Variable     |   Valeur   |
| :-------------: | :--------: |
|     Système     | PostgreSQL |
|     Serveur     |  postgres  |
|   Utilisateur   |  postgres  |
|  Mot de passe   |  postgres  |
| Base de données |  postgres  |

### Bus de données Kafka

| Variable |    Valeur    |
| :------: | :----------: |
|  Topic   | racing-notif |

### Fichier /etc/hosts

Pour tester l'application Spring Boot depuis l'IDE en utilisant les composants montés dans le docker-compose, il est nécessaire d'ajouter la section suivante dans le fichier `/etc/hosts` :

```
172.26.1.0      webserver
172.26.1.1      zookeeper
172.26.1.2      kafka
172.26.1.3      postgres
172.26.1.4      adminer
```

## Tests

### Makefile

Un `Makefile` propose les différentes commandes de gestion. La commande par défaut est `build`.

```sh
make help
```

Il permet de démarrer l'environnement en utilisant [`docker-compose`](https://docs.docker.com/engine/reference/commandline/compose/). L'environnement est constitué de :

- [Apache Kafka](https://kafka.apache.org/),
- [Apache ZooKeeper](https://zookeeper.apache.org/) utilisé par Kafka,
- [PostgreSQL](https://www.postgresql.org/),
- [Adminer](https://www.adminer.org/), IHM de gestion/visualisation de base de données.
- Le web server applicatif (optionnellement)

Pour démarrer :

```sh
make up
```

Pour arrêter :

```sh
make down
```

Pour visualiser l'arrivée de messages sur le bus Kafka (en utilisant l'outils `kafka-console-consumer.sh` inclus dans le docker Kafka) :

```sh
make consume
```

Pour visualiser le stockage dans la base de données, ouvrir l'outil [Adminer](http://172.26.1.4:8080/) dans un navigateur.

### Lancement de l'application hors `docker-compose`

Dans ce mode, il est nécessaire de commenter préalablement `webserver` dans le fichier `docker-compose.yml` avant de démarrer l'environnement.

Pour générer l'application, notamment le JAR :

```sh
make package
```

Pour démarrer l'application à partir du JAR généré :

```sh
make run
```
