package fr.arno750.spkws;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import fr.arno750.spkws.model.Race;
import fr.arno750.spkws.model.RaceId;
import fr.arno750.spkws.repository.RaceRepository;
import fr.arno750.spkws.service.RaceAlreadyExistException;
import fr.arno750.spkws.service.RaceService;

/**
 * This class contains unit tests.
 *
 */
@ExtendWith(MockitoExtension.class)
public class RaceServiceTest {

	@InjectMocks
	private RaceService raceService;

	@Mock
	private RaceRepository raceRepository;

	@Mock
	private KafkaTemplate<String, Race> kafkaTemplate;

	@Captor
	ArgumentCaptor<RaceId> raceIdCaptor;

	@Captor
	ArgumentCaptor<Race> raceCaptor;

	@Test
	public void givenFound_whenExists() throws Exception {
		// Arrange
		final var raceToCheck = Race.builder().name("first").number(1234).date(new Date()).build();
		when(raceRepository.existsById(raceIdCaptor.capture())).thenReturn(true);

		// Act
		final var actual = raceService.exists(raceToCheck);

		// Assert
		assertTrue(actual);
		assertThat(raceIdCaptor.getValue().getNumber()).isEqualTo(raceToCheck.getNumber());
		assertThat(raceIdCaptor.getValue().getDate()).isEqualTo(raceToCheck.getDate());
		verify(raceRepository, times(1)).existsById(any(RaceId.class));
		verifyNoMoreInteractions(raceRepository);
	}

	@Test
	public void givenNotFound_whenExists() throws Exception {
		// Arrange
		when(raceRepository.existsById(any(RaceId.class))).thenReturn(false);

		// Act
		final var actual = raceService.exists(new Race());

		// Assert
		assertFalse(actual);
		verify(raceRepository, times(1)).existsById(any(RaceId.class));
		verifyNoMoreInteractions(raceRepository);
	}

	@Test
	public void whenSave() throws Exception {
		// Arrange
		final var raceToSave = Race.builder().name("first").number(1234).date(new Date()).build();
		when(raceRepository.save(any(Race.class))).thenReturn(raceToSave);

		// Act
		final var actual = raceService.save(new Race());

		// Assert
		assertThat(actual).usingRecursiveComparison().isEqualTo(raceToSave);
		verify(raceRepository, times(1)).save(any(Race.class));
		verifyNoMoreInteractions(raceRepository);
	}

	@Test
	public void whenSend() throws Exception {
		// Arrange
		final var raceToSend = Race.builder().name("first").number(1234).date(new Date()).build();
		when(kafkaTemplate.send(any(), any(), raceCaptor.capture())).thenReturn(null);

		// Act
		raceService.send(raceToSend);

		// Assert
		assertThat(raceCaptor.getValue()).usingRecursiveComparison().isEqualTo(raceToSend);
		verify(kafkaTemplate, times(1)).send(any(), any(), any(Race.class));
		verifyNoMoreInteractions(kafkaTemplate);
	}

	@Test
	public void givenNotFound_whenCreate() throws Exception {
		// Arrange
		when(raceRepository.existsById(any(RaceId.class))).thenReturn(false);

		// Act
		raceService.create(new Race());

		// Assert
		verify(raceRepository, times(1)).existsById(any(RaceId.class));
		verify(raceRepository, times(1)).save(any(Race.class));
		verifyNoMoreInteractions(raceRepository);
		verify(kafkaTemplate, times(1)).send(any(), any(), any(Race.class));
		verifyNoMoreInteractions(kafkaTemplate);
	}

	@Test
	public void givenFound_whenCreate_thenThrow() throws Exception {
		// Arrange
		when(raceRepository.existsById(any(RaceId.class))).thenReturn(true);

		// Act
		assertThrows(RaceAlreadyExistException.class, () -> raceService.create(new Race()));

		// Assert
		verify(raceRepository, times(1)).existsById(any(RaceId.class));
		verify(raceRepository, times(0)).save(any(Race.class));
		verifyNoMoreInteractions(raceRepository);
		verify(kafkaTemplate, times(0)).send(any(), any(), any(Race.class));
		verifyNoMoreInteractions(kafkaTemplate);
	}
}