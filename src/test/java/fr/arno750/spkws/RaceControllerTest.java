package fr.arno750.spkws;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.FileCopyUtils;

import fr.arno750.spkws.controller.RaceController;
import fr.arno750.spkws.model.Race;
import fr.arno750.spkws.repository.RaceRepository;
import fr.arno750.spkws.service.RaceAlreadyExistException;
import fr.arno750.spkws.service.RaceService;

/**
 * This class contains unit tests.
 *
 */
@WebMvcTest(RaceController.class)
public class RaceControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private RaceRepository repository;

	@MockBean
	private RaceService service;

	/**
	 * Reads a resource file and returns the content as a {@link java.lang.String
	 * String}.
	 * 
	 * @param sampleName
	 * @return the content of the sample as a {@link java.lang.String String}
	 */
	private String getSample(String sampleName) {
		ResourceLoader resourceLoader = new DefaultResourceLoader();
		Resource resource = resourceLoader.getResource(sampleName);
		try (Reader reader = new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8)) {
			return FileCopyUtils.copyToString(reader);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	@Test
	public void postRace_thenStatus200_andNotification() throws Exception {
		this.mockMvc.perform(post("/api/race").contentType(MediaType.APPLICATION_JSON).content(getSample("test1.json")))
				.andExpect(status().isCreated());
		verify(service, times(1)).create(any(Race.class));
	}

	@Test
	public void postRace_thenStatus400_onAlreadyExists() throws Exception {
		doThrow(RaceAlreadyExistException.class).when(service).create(any());
		this.mockMvc.perform(post("/api/race").contentType(MediaType.APPLICATION_JSON).content(getSample("test1.json")))
				.andExpect(status().is4xxClientError());
	}

	@Test
	public void postRace_thenStatus400_onInvalidRunnerCount() throws Exception {
		this.mockMvc.perform(post("/api/race").contentType(MediaType.APPLICATION_JSON).content(getSample("test2.json")))
				.andExpect(status().is4xxClientError());
	}

	@Test
	public void postRace_thenStatus400_onInvalidRunnerNumberOrder() throws Exception {
		this.mockMvc.perform(post("/api/race").contentType(MediaType.APPLICATION_JSON).content(getSample("test3.json")))
				.andExpect(status().is4xxClientError());
	}
}
