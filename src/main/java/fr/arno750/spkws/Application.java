package fr.arno750.spkws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * The main class of the application.
 *
 */
@EnableJpaRepositories("fr.arno750.spkws.repository")
@EntityScan("fr.arno750.spkws.model")
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		System.out.println("SPKWS");
		SpringApplication.run(Application.class, args);
	}
}
