package fr.arno750.spkws.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.arno750.spkws.model.Race;
import fr.arno750.spkws.service.RaceAlreadyExistException;
import fr.arno750.spkws.service.RaceService;

/**
 * This class is a controller. It handles the requests on races.
 *
 */
@RestController
@RequestMapping("/api/race")
public class RaceController {

	@Autowired
	private RaceService raceService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void create(@Valid @RequestBody Race race) {
		try {
			raceService.create(race);
		} catch (RaceAlreadyExistException exception) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Race already exists", exception);
		}
	}
}
