package fr.arno750.spkws.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * This class is an entity.
 * 
 * @see RunnerId
 * @see Race
 */
@Entity
@Getter @Setter @EqualsAndHashCode
@IdClass(RunnerId.class)
public class Runner implements Comparable<Runner> {

	@NotNull
	private String name;

	@Id
	@NotNull
	private Integer number;

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private Race race;

	@Override
	public int compareTo(Runner other) {
		return number.compareTo(other.number);
	}
}
