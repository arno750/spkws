package fr.arno750.spkws.model.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import fr.arno750.spkws.model.Race;
import fr.arno750.spkws.model.Runner;

/**
 * This defines the annotation @ValidOrderedRunners for the
 * {@link ValidOrderedRunnersValidator validator} on {@link Race race}
 * {@link Runner runners}.
 * 
 */
@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidOrderedRunnersValidator.class)
@Documented
public @interface ValidOrderedRunners {

	String message() default "{ValidOrderedRunners.invalid}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
