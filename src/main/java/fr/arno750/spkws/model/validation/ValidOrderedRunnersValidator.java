package fr.arno750.spkws.model.validation;

import java.util.Collections;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import fr.arno750.spkws.model.Race;
import fr.arno750.spkws.model.Runner;

/**
 * This class is a constraint validator.
 * 
 * It is checking that the specified {@link Race race} contains n runners with
 * numbers going from 1 to n without any gap.
 * 
 * It is not required that the runners be sorted.
 * 
 * @see Race
 * @see Runner
 */
public class ValidOrderedRunnersValidator implements ConstraintValidator<ValidOrderedRunners, Race> {

	@Override
	public void initialize(ValidOrderedRunners constraintAnnotation) {
	}

	@Override
	public boolean isValid(Race race, ConstraintValidatorContext context) {
		List<Runner> runners = race.getRunners();

		// Sort the runner by number
		Collections.sort(runners);

		// Check the runner number starts from 1 and are incrementing
		int number = 1;
		for (Runner runner : runners) {
			if (number != runner.getNumber()) {
				return false;
			}
			number++;
		}

		return true;
	}
}
