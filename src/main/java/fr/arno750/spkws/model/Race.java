package fr.arno750.spkws.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.sun.istack.NotNull;

import fr.arno750.spkws.model.validation.ValidOrderedRunners;
import fr.arno750.spkws.model.validation.ValidOrderedRunnersValidator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class is the main entity. It contains a list of {@link Runner runners}.
 * 
 * The order of the runner numbers is validated by a custom
 * {@link ValidOrderedRunnersValidator validator}.
 * 
 * @see RaceId
 * @see ValidOrderedRunnersValidator
 * @see Runner
 */
@Entity
@Getter @Setter @EqualsAndHashCode
@Builder @NoArgsConstructor @AllArgsConstructor
@IdClass(RaceId.class)
@ValidOrderedRunners
public class Race {

	@Id
	@NotNull
	@Temporal(TemporalType.DATE)
	private Date date;

	@Id
	@NotNull
	private Integer number;

	@NotNull
	@NotBlank(message = "Name is mandatory")
	private String name;

	@OneToMany(mappedBy = "race", cascade = CascadeType.ALL, orphanRemoval = true)
	@Size(min = 3, message = "Minimum 3 runners")
	private final List<Runner> runners = new ArrayList<>();

	/**
	 * @param runners the runners to set
	 */
	public void setRunners(List<Runner> runners) {
		this.runners.addAll(runners);
		for (Runner runner : this.runners) {
			runner.setRace(this);
		}
	}

	/**
	 * @param runner
	 */
	public void addRunner(Runner runner) {
		runners.add(runner);
		runner.setRace(this);
	}

	/**
	 * @param runner
	 */
	public void removeRunner(Runner runner) {
		runners.remove(runner);
		runner.setRace(null);
	}
}
