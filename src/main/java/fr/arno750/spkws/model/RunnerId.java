package fr.arno750.spkws.model;

import java.io.Serializable;

/**
 * This class is a composite primary key for the {@link Runner runner} entity
 * based on the runner number and the associated parent race.
 *
 */
public class RunnerId implements Serializable {

	private static final long serialVersionUID = 1457266846015728098L;

	private Integer number;

	private Race race;

	public RunnerId() {
	}

	public RunnerId(Integer number, Race race) {
		this.number = number;
		this.race = race;
	}

	/**
	 * @return the number
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * @return the race
	 */
	public Race getRace() {
		return race;
	}

	/**
	 * @param race the race to set
	 */
	public void setRace(Race race) {
		this.race = race;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((race == null) ? 0 : race.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RunnerId other = (RunnerId) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (race == null) {
			if (other.race != null)
				return false;
		} else if (!race.equals(other.race))
			return false;
		return true;
	}
}
