package fr.arno750.spkws.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.arno750.spkws.model.Race;
import fr.arno750.spkws.model.RaceId;

/**
 * This class is a CRUD repository for the {@link Race race} entity.
 *
 */
public interface RaceRepository extends CrudRepository<Race, RaceId> {
	List<Race> findAll();
}
