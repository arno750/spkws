package fr.arno750.spkws.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import fr.arno750.spkws.model.Race;
import fr.arno750.spkws.model.RaceId;
import fr.arno750.spkws.repository.RaceRepository;

/**
 * This class is a service. It communicates with the repository and the message
 * broker.
 *
 */
@Service
public class RaceService {

	private final Logger logger = LoggerFactory.getLogger(RaceService.class);

	public static final String TOPIC = "racing-notif";

	@Autowired
	private RaceRepository raceRepository;

	@Autowired
	private KafkaTemplate<String, Race> kafkaTemplate;

	/**
	 * @param race
	 * @return
	 */
	public boolean exists(Race race) {
		return raceRepository.existsById(new RaceId(race.getDate(), race.getNumber()));
	}

	/**
	 * @param race
	 * @return
	 */
	public Race save(Race race) {
		return raceRepository.save(race);
	}

	/**
	 * @param race
	 */
	public void send(Race race) {
		kafkaTemplate.send(TOPIC, race.getDate() + "-" + race.getNumber(), race);
	}

	/**
	 * @param race
	 * @throws RaceAlreadyExistException
	 */
	public void create(Race race) throws RaceAlreadyExistException {
		if (exists(race)) {
			logger.debug("Race {} already exist on {}", race.getNumber(), race.getDate());
			throw new RaceAlreadyExistException();
		}

		save(race);
		send(race);
	}
}
