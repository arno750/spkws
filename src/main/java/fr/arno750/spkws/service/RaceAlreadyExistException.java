package fr.arno750.spkws.service;

/**
 * This class is a exception indicating a race already exists.
 *
 */
public class RaceAlreadyExistException extends Exception {
	
	private static final long serialVersionUID = 9032967667351358449L;

	public RaceAlreadyExistException() {
		super();
	}

	public RaceAlreadyExistException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public RaceAlreadyExistException(final String message) {
		super(message);
	}

	public RaceAlreadyExistException(final Throwable cause) {
		super(cause);
	}
}